/*!
 * @file cxmppx.h 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*!
 * \mainpage
 * cxmppx is a lib for XMPP XEPs.
 *
 */

/*!
 * \mainpage
 * cxmppx is a lib for XMPP XEPs.
 *
 */

#ifndef __LIBCXMPPX_CXMPPX_H__
#define __LIBCXMPPX_CXMPPX_H__

#include <strophe.h>

// ============================================================================
// Stanza Name
// ============================================================================

#define CXMPPX_STANZA_NAME_QUERY    "query"
#define CXMPPX_STANZA_NAME_PUBSUB   "pubsub"
#define CXMPPX_STANZA_NAME_SUBSCRIPTIONS   "subscriptions"
#define CXMPPX_STANZA_NAME_ITEMS    "items"
#define CXMPPX_STANZA_NAME_ITEM     "item"
#define CXMPPX_STANZA_NAME_OPENPGP  "openpgp"
#define CXMPPX_STANZA_NAME_SIGNCRYPT  "signcrypt"
#define CXMPPX_STANZA_NAME_TO  "to"
#define CXMPPX_STANZA_NAME_FROM  "from"
#define CXMPPX_STANZA_NAME_TIME  "time"
#define CXMPPX_STANZA_NAME_RPAD  "rpad"
#define CXMPPX_STANZA_NAME_PAYLOAD  "payload"
#define CXMPPX_STANZA_NAME_BODY  "body"

// ============================================================================
// Stanza Type
// ============================================================================

#define CXMPPX_STANZA_TYPE_GET      "get"
#define CXMPPX_STANZA_TYPE_SET      "set"
#define CXMPPX_STANZA_TYPE_ERROR    "error"

// ============================================================================
// Stanza Attribute
// ============================================================================

#define CXMPPX_STANZA_ATTRIBUTE_NODE      "node"
#define CXMPPX_STANZA_ATTRIBUTE_JID       "jid"
#define CXMPPX_STANZA_ATTRIBUTE_STAMP      "stamp"
#define CXMPPX_STANZA_ATTRIBUTE_NAME      "name"

// ============================================================================
// Namespaces
// ============================================================================

#define CXMPPX_NS_DISCO_INFO "http://jabber.org/protocol/disco#info"
#define CXMPPX_NS_PUBSUB     "http://jabber.org/protocol/pubsub"
#define CXMPPX_NS_OPENPGP_0  "urn:xmpp:openpgp:0"
#define CXMPPX_NS_JABBER_CLIENT  "jabber:client"
#define CXMPPX_NS_JABBER_IQ_ROSTER "jabber:iq:roster"
#define CXMPPX_NS_CARBONS_2  "urn:xmpp:carbons:2"

#define CXMPPX_NODE_OPENPGP_0_PUBLIC_KEYS "urn:xmpp:openpgp:0:public-keys"

// ============================================================================
// API 
// ============================================================================

/*!
 * \brief xmpp_adr_t is a XMPP ID.
 *
 * typedef of char* for an XMPP-ID.
 *
 * bare XMPP-ID: <localpart@domainpart> (account) or <domainpart> (server)
 * full XMPP-ID: <localpart@domainpart/resourcepart> or <domainpart/resourcepart>
 *
 * https://tools.ietf.org/html/rfc6120#section-1.4
 * https://tools.ietf.org/html/rfc6122
 */

typedef char* xmpp_adr_t;

/*!
 * \brief xmpp stanza ID.
 *
 * typdef of char* for a Stanza ID
 */

typedef char* stanza_id_t;

/*!
 * \brief Struct to hold a stanza and the stanza's id.
 *
 */

typedef struct {
  stanza_id_t id;
  xmpp_stanza_t *stanza;
} cxmppx_stanza_t;


/*!
 * \brief Callback for stanza result handling.
 *
 * The cxmppx framework provided a function fire_and_handle to send a stanza to
 * a server. If the server sends the reponse, the cxmppx framework will call the
 * callback function.
 *
 * @param stanza The stanza of the result
 * @param obj not in use (business object - see below)
 * @param userdata callback user data
 *
 */

typedef void (*cxmppx_stanza_result_cb_t)(xmpp_stanza_t *stanza, void *const obj, void *const userdata);

/*!
 * \brief Business object - Account.
 *
 * Account is the XMPP-ID used by the client.
 *
 */
typedef struct _cxmppx_account_t cxmppx_account_t;

/*!
 * \brief Business object - XMPP-ID.
 *
 * Holds information of a XMPP-ID. This object can be used to store information
 * about server, accounts, groupchats,..).
 '
 ' I depends on the context, what will be stored within this business object.
 *
 * Each XMPP-ID object is assigned to an XMPP Account (cxmppx_account_t), which
 * represents the local XMPP account for multi account support.
 *
 * If there are two XMPP-Accounts in use, and both accounts reference to one
 * XMPP-ID (e.g. both accounts have the same account in his roster), the XMPP-ID
 * exists two times (once per account).
 */
typedef struct _cxmppx_xmpp_entity_t cxmppx_xmpp_entity_t;

/*!
 * \brief Business object - Roster
 *
 * Roster is a list of MPP-ID Business objects.
 * A roster is linked to one account.
 *
 */
typedef struct _cxmppx_roster_t cxmppx_roster_t;

typedef enum {
    CXMPPX_DISCONNECTED, 
    CXMPPX_CONNECTING, 
    CXMPPX_CONNECTED
} cxmppx_connection_state_t;


typedef enum {
  CXMPPX_OFFLINE,
  CXMPPX_ONLINE,
  CXMPPX_CHAT,
  CXMPPX_AWAY,
  CXMPPX_XA,
  CXMPPX_DND,
} cxmppx_presence_state_t;

// ----------------------------------------------------------------------------
// Account - (business object)
// ----------------------------------------------------------------------------

cxmppx_account_t * cxmppx_create_account(xmpp_conn_t *const conn);

cxmppx_roster_t * cxmppx_account_get_roster(cxmppx_account_t* const account);

cxmppx_xmpp_entity_t * cxmppx_create_xmpp_entity(cxmppx_account_t * account, xmpp_adr_t adr);

xmpp_adr_t cxmppx_xmpp_entity_get_xmppadr(cxmppx_xmpp_entity_t* entity);

char* cxmppx_xmpp_entity_get_name(cxmppx_xmpp_entity_t* entity);

char* cxmppx_xmpp_entity_get_presence_status(cxmppx_xmpp_entity_t* entity);
cxmppx_presence_state_t cxmppx_xmpp_entity_get_presence_state(cxmppx_xmpp_entity_t* entity);

cxmppx_xmpp_entity_t* cxmppx_xmpp_entity_next(cxmppx_xmpp_entity_t* entity);

cxmppx_roster_t * cxmppx_build_roster(cxmppx_account_t* const account, cxmppx_stanza_t* stanza );

cxmppx_xmpp_entity_t* cxmppx_roster_items(cxmppx_roster_t* roster);


// ----------------------------------------------------------------------------
// stanza
// ----------------------------------------------------------------------------

void fire_and_handle(xmpp_conn_t *const conn, cxmppx_stanza_t* cxmppx_stanza, cxmppx_stanza_result_cb_t callback, void *const userdata);

void register_handler(xmpp_conn_t *const conn, cxmppx_account_t* account );

// ----------------------------------------------------------------------------
// RFC-6121 
// ----------------------------------------------------------------------------

void cxmppx_roster_query_get(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn);

void cxmppx_presence(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn);

// ----------------------------------------------------------------------------
// XEP-0030 - Service Discovery (disco)
// ----------------------------------------------------------------------------

void cxmppx_disco_query_info(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn, xmpp_adr_t xmpp_adr);

// ----------------------------------------------------------------------------
// XEP-0060 - Publish-Subscribe (pubsub)
// ----------------------------------------------------------------------------

void cxmppx_pubsub_query_subscriptions(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn, xmpp_adr_t xmpp_adr);
void cxmppx_pubsub_requests_items(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn, xmpp_adr_t xmpp_adr, char* node);

// ----------------------------------------------------------------------------
// XEP-0280: Message Carbons
// ----------------------------------------------------------------------------

void cxmppx_carbone(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn);

// ----------------------------------------------------------------------------
// XEP-0373: OpenPGP for XMPP (ox)
// ----------------------------------------------------------------------------

/*!
 * \brief Discovering Public Keys of a User (metadata)
 *
 * https://xmpp.org/extensions/xep-0373.html#discover-pubkey-list
 *
 * @param cxmppx_stanza 
 * @param conn XMPP connection
 * @param xmpp_adr XMPP Address of the contact 
 */
void cxmppx_ox_discover_metadata(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn, xmpp_adr_t xmpp_adr);


void cxmppx_ox_signcrypt_message(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn, xmpp_adr_t xmpp_adr, char* message);


// ----------------------------------------------------------------------------
// 0420 - Stanza Content Encryption (sce)
// ----------------------------------------------------------------------------

// xmpp_stanza_t *cxmppx_sce_create_envelope();

xmpp_stanza_t *  cxmppx_sce_create_content(xmpp_conn_t *conn, xmpp_adr_t xmpp_adr_to, xmpp_adr_t xmpp_adr_from);


#endif // __LIBCXMPPX_CXMPPX_H__

