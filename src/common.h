/*!
 * @file common.h 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBCXMPPX_COMMON_H__
#define __LIBCXMPPX_COMMON_H__

#include "../cxmppx.h"

typedef struct {
  void* userdata;
  cxmppx_stanza_result_cb_t callback;
} user_data_cb_t;

char* bare_address(char* address);

#endif // __LIBCXMPPX_COMMON_H__

