/*!
 * @file account.c 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "account.h"
#include "../cxmppx.h"
#include "common.h"
#include <stdlib.h>

cxmppx_account_t * cxmppx_create_account(xmpp_conn_t *const conn){
  cxmppx_account_t* account = malloc(sizeof(cxmppx_account_t));
  account->roster = NULL;
  account->known_server = NULL;
  account->known_accounts = NULL;
  return account;
}

cxmppx_xmpp_entity_t * cxmppx_create_xmpp_entity(cxmppx_account_t * account, xmpp_adr_t xmpp_adr) {
  cxmppx_xmpp_entity_t* xmppentity = malloc(sizeof(cxmppx_xmpp_entity_t));
  xmppentity->xmpp_adr = strdup(xmpp_adr);
  xmppentity->presence_state = CXMPPX_OFFLINE;
  xmppentity->presence_status = NULL;
  xmppentity->name = NULL;
  xmppentity->next = NULL;
  return xmppentity;
}

cxmppx_roster_t * cxmppx_build_roster(cxmppx_account_t* const account, cxmppx_stanza_t* stanza ){
  cxmppx_roster_t* roster = malloc(sizeof(cxmppx_roster_t));
  roster->item = NULL;

  xmpp_stanza_t *query, *item;
  const char *type = NULL; 
  type = xmpp_stanza_get_type(stanza->stanza);

  if (type != NULL && strcmp(type, CXMPPX_STANZA_TYPE_ERROR) == 0) {
    return; 
  } else {
    cxmppx_xmpp_entity_t** last = &(roster->item);
    query = xmpp_stanza_get_child_by_name(stanza->stanza, CXMPPX_STANZA_NAME_QUERY );
    for (item = xmpp_stanza_get_children(query); item;
         item = xmpp_stanza_get_next(item)) {
      xmpp_adr_t adr = xmpp_stanza_get_attribute(item, CXMPPX_STANZA_ATTRIBUTE_JID);
      char* name = xmpp_stanza_get_attribute(item, CXMPPX_STANZA_ATTRIBUTE_NAME);
      cxmppx_xmpp_entity_t * entity = cxmppx_create_xmpp_entity(account, adr);
      *last = entity;
      last = &(entity->next);
    }
  }

  account->roster = roster;
  return roster;
}

cxmppx_roster_t * cxmppx_account_get_roster(cxmppx_account_t* const account) {
  return account->roster;
}


xmpp_adr_t cxmppx_xmpp_entity_get_xmppadr(cxmppx_xmpp_entity_t* entity) {
  return entity->xmpp_adr;
}

char* cxmppx_xmpp_entity_get_name(cxmppx_xmpp_entity_t* entity) {
  return entity->name;
}

char* cxmppx_xmpp_entity_get_presence_status(cxmppx_xmpp_entity_t* entity){
  return entity->presence_status;
}

cxmppx_presence_state_t cxmppx_xmpp_entity_get_presence_state(cxmppx_xmpp_entity_t* entity) {
  return entity->presence_state;
}

cxmppx_xmpp_entity_t* cxmppx_roster_items(cxmppx_roster_t* roster) {
  return roster->item;
}

cxmppx_xmpp_entity_t* cxmppx_xmpp_entity_next(cxmppx_xmpp_entity_t* entity) {
  return entity->next;
}

void cxmppx_roster_query_get(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn) {
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  cxmppx_stanza->id = xmpp_uuid_gen(ctx);
  xmpp_stanza_t *stanza = xmpp_iq_new(ctx, CXMPPX_STANZA_TYPE_GET, cxmppx_stanza->id);
  cxmppx_stanza->stanza = stanza;
  xmpp_stanza_t *query = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(query, CXMPPX_STANZA_NAME_QUERY);
  xmpp_stanza_set_ns(query, CXMPPX_NS_JABBER_IQ_ROSTER);
  xmpp_stanza_add_child(stanza, query);
}

void cxmppx_presence(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn) {
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  cxmppx_stanza->id = xmpp_uuid_gen(ctx);
  xmpp_stanza_t *presence = xmpp_presence_new(ctx);
  cxmppx_stanza->stanza = presence;
  xmpp_stanza_set_id(presence, cxmppx_stanza->id);
}

void cxmppx_carbone(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn) {
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  cxmppx_stanza->id = xmpp_uuid_gen(ctx);
  xmpp_stanza_t *carbons = xmpp_iq_new(ctx, CXMPPX_STANZA_TYPE_SET, cxmppx_stanza->id);
  cxmppx_stanza->stanza = carbons;
  xmpp_stanza_t *enable = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(enable, "enable");
  xmpp_stanza_set_ns(enable, CXMPPX_NS_CARBONS_2);
  xmpp_stanza_add_child(carbons, enable);
}



