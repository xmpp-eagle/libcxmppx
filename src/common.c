/*!
 * @file common.c 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "common.h"
#include "account.h"
//#include <strophe.h>
#include <stdlib.h>
#include <string.h>

static _stanza_handle(xmpp_conn_t *const conn, xmpp_stanza_t *const stanza, void *const userdata);

void fire_and_handle(xmpp_conn_t *const conn, cxmppx_stanza_t* cxmppx_stanza, cxmppx_stanza_result_cb_t callback, void *const userdata) {

  user_data_cb_t* user_data_cb = malloc(sizeof(user_data_cb_t));
  user_data_cb->userdata = userdata;
  user_data_cb->callback = callback; 

  xmpp_id_handler_add(conn, _stanza_handle ,cxmppx_stanza->id, user_data_cb);
  xmpp_send(conn, cxmppx_stanza->stanza);  
}


int presence(xmpp_conn_t *const conn, xmpp_stanza_t *const stanza, void *const userdata) {
  cxmppx_account_t* account = userdata;

  char* from = xmpp_stanza_get_from(stanza);
  char* bare = bare_address(from);

  cxmppx_roster_t* roster = cxmppx_account_get_roster(account);
  if( roster )  {
    cxmppx_xmpp_entity_t* entity = cxmppx_roster_items(roster);
    while ( entity ) {
      if(strcmp(entity->xmpp_adr, bare) == 0){
        entity->presence_state = CXMPPX_ONLINE;
      }
      entity = cxmppx_xmpp_entity_next(entity);
    }
  }

  return 1;
}

void register_handler(xmpp_conn_t *const conn, cxmppx_account_t* account ) {
  char* ns = NULL;
  char* type = NULL;
  xmpp_handler_add(conn,presence, ns, "presence", type, account );
}

char* bare_address(char* address) {
  if(address) {
    return strtok(address, "/");
  }
  return NULL;
}

static _stanza_handle(xmpp_conn_t *const conn, xmpp_stanza_t *const stanza, void *const userdata) {
  user_data_cb_t* user_data_cb = (user_data_cb_t*) userdata;
  char* text;
  size_t textlen;
  xmpp_stanza_to_text(stanza,&text, &textlen);
  if( user_data_cb->callback ) {
    user_data_cb->callback(stanza , text , user_data_cb->userdata);
  }
}

