/*!
 * @file account.h 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBCXMPPX_ACCOUNT_H__
#define __LIBCXMPPX_ACCOUNT_H__

#include "../cxmppx.h"

// XMPP entity
struct _cxmppx_xmpp_entity_t { 
  xmpp_adr_t xmpp_adr;
  char* name;
  cxmppx_presence_state_t presence_state;
  cxmppx_xmpp_entity_t* next;
  char* presence_status;
};

// Account
struct _cxmppx_account_t{ 
  cxmppx_xmpp_entity_t* known_server;
  cxmppx_xmpp_entity_t* known_accounts;
  cxmppx_roster_t *roster;
};

// Roster
struct _cxmppx_roster_t {
  cxmppx_xmpp_entity_t* item;
}; 

#endif // __LIBCXMPPX_ACCOUNT_H__

