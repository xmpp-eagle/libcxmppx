/*!
 * @file 0420-sce.h 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LIBCXMPPX_0420_SCE_H__
#define __LIBCXMPPX_0420_SCE_H__

char* generate_rpad();

#endif // __LIBCXMPPX_0420_SCE_H__

