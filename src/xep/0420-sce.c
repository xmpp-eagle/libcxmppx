/*!
 * @file 0420-sce.c 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "0420-sce.h"
#include "../../cxmppx.h"
#include "../rfc/4648-base64.h"
#include <strophe.h>
#include <stdlib.h>
#include <time.h>

static char* _generate_rpad();

static unsigned int seed = 0;

char* generate_rpad() {
  return _generate_rpad();
}

xmpp_stanza_t * cxmppx_sce_create_content(xmpp_conn_t *conn, xmpp_adr_t xmpp_adr_to, xmpp_adr_t xmpp_adr_from) {
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  // signcrypt
  xmpp_stanza_t *signcrypt = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(signcrypt, "signcrypt");
  xmpp_stanza_set_ns(signcrypt, "urn:xmpp:openpgp:0");
  // to
  xmpp_stanza_t *s_to = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(s_to, "to");
  xmpp_stanza_set_attribute(s_to, "jid",xmpp_adr_to);
  // time
  xmpp_stanza_t *time = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(time, "time");
//FIXME  xmpp_stanza_set_attribute(time, "stamp", buf);
  xmpp_stanza_set_name(time, "time");
  // rpad
  xmpp_stanza_t *rpad = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(rpad, "rpad");
  xmpp_stanza_t *rpad_text = xmpp_stanza_new(ctx);
  xmpp_stanza_set_text(rpad_text, _generate_rpad());
  // payload
  xmpp_stanza_t *payload= xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(payload, "payload");
  // body
  xmpp_stanza_t *body = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(body, "body");
  xmpp_stanza_set_ns(body, "jabber:client");
  // text
  xmpp_stanza_add_child(signcrypt,s_to);
  xmpp_stanza_add_child(signcrypt,time);
  xmpp_stanza_add_child(signcrypt,rpad);
  xmpp_stanza_add_child(rpad,rpad_text);
  xmpp_stanza_add_child(signcrypt,payload);
  xmpp_stanza_add_child(payload, body);
  return signcrypt;

}


char* _generate_rpad() {
  if ( !seed) seed = time(NULL);
  int size = (rand_r(&seed) % 201) ;
  if(size > 0 ) {
    char* rpad = malloc( sizeof(char) * (size+1));
    rpad[size] = '\0';
    for(int i = 0;i < size; i++)
      rpad[i] = rfc_4648_base64_alphabet[(rand_r(&seed)%64)];
    return rpad;
  }
  return NULL;
}

