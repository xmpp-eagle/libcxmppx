/*!
 * @file 0030-disco.c 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "0030-disco.h"
#include "../../cxmppx.h"
#include "../common.h"
#include <stdlib.h>

void cxmppx_disco_query_info(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn, xmpp_adr_t xmpp_adr){
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  cxmppx_stanza->id = xmpp_uuid_gen(ctx);
  xmpp_stanza_t *stanza = xmpp_iq_new(ctx, CXMPPX_STANZA_TYPE_GET, cxmppx_stanza->id);
  cxmppx_stanza->stanza = stanza;
  xmpp_stanza_set_to(stanza,xmpp_adr);
  xmpp_stanza_set_from(stanza, xmpp_conn_get_jid(conn));
  xmpp_stanza_t *query = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(query, CXMPPX_STANZA_NAME_QUERY);
  xmpp_stanza_set_ns(query,CXMPPX_NS_DISCO_INFO);
  xmpp_stanza_add_child(stanza, query);
}

