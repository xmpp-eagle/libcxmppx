/*!
 * @file 0373-ox.c 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "../common.h"
#include "0373-ox.h"
#include "0420-sce.h"
#include <time.h> // move to 0082-time
#include "../ext/openpgp.h"
#include <stdio.h>

static xmpp_stanza_t *_openpgp_signcrypt_stanza(xmpp_conn_t *conn, xmpp_adr_t xmpp_adr, char* message);

void cxmppx_ox_discover_metadata(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn, xmpp_adr_t xmpp_adr) {
  cxmppx_pubsub_requests_items(cxmppx_stanza, conn, xmpp_adr, CXMPPX_NODE_OPENPGP_0_PUBLIC_KEYS);
}

/*!
 * \brief Send OX signcrypt message
 */

void cxmppx_ox_signcrypt_message(cxmppx_stanza_t* cxmppx_stanza, xmpp_conn_t *conn, xmpp_adr_t xmpp_adr, char* message){
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  cxmppx_stanza->id = xmpp_uuid_gen(ctx);
  xmpp_stanza_t *stanza = xmpp_message_new(xmpp_conn_get_context(conn), NULL, xmpp_adr, cxmppx_stanza->id);
  cxmppx_stanza->stanza = stanza;
  xmpp_message_set_body(stanza, "This message is *encrypted* with OpenPGP (See XEP-0373)");
  // xmpp_stanza_set_type(*stanza, "chat");
  xmpp_stanza_t *openpgp = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(openpgp, CXMPPX_STANZA_NAME_OPENPGP );
  xmpp_stanza_set_ns(openpgp, CXMPPX_NS_OPENPGP_0);

  // Build content stanza 
  xmpp_stanza_t *signcrypt = _openpgp_signcrypt_stanza(conn, xmpp_adr, message);
 
  gpgme_key_t key_to = openpgp_lookup_key(xmpp_adr);
  if( !key_to ) {
    printf("Key for %s not found\n", xmpp_adr);
  }
  gpgme_key_t key_from = openpgp_lookup_key(xmpp_conn_get_jid(conn));
  if( !key_from) {
    printf("Key for %s not found\n", xmpp_conn_get_jid(conn));
  }

  char* error = NULL;
  gpgme_key_t r[2];
  r[0]=key_to;
  r[1]=NULL;
  char* signcrypt_e = openpgp_signcrypt(key_from, r, message, &error);

  // BASE64_OPENPGP_MESSAGE
  xmpp_stanza_t *base64_openpgp_message = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_text(base64_openpgp_message, signcrypt_e);
  xmpp_stanza_add_child(openpgp, base64_openpgp_message);
  xmpp_stanza_add_child(stanza, openpgp);
}

static xmpp_stanza_t *_openpgp_signcrypt_stanza(xmpp_conn_t *conn, xmpp_adr_t xmpp_adr, char* message) {
  time_t now = time(NULL);
  struct tm *tm = localtime(&now);
  char buf[255];
  strftime(buf, sizeof(buf), "%FT%T%z", tm);
  char* rpad_data = generate_rpad();

  // signcrypt
  xmpp_stanza_t *signcrypt = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(signcrypt, CXMPPX_STANZA_NAME_SIGNCRYPT);
  xmpp_stanza_set_ns(signcrypt, CXMPPX_NS_OPENPGP_0);
  xmpp_stanza_t *s_to = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(s_to, CXMPPX_STANZA_NAME_TO);
  xmpp_stanza_set_attribute(s_to, CXMPPX_STANZA_ATTRIBUTE_JID, xmpp_adr);
  // time
  xmpp_stanza_t *time = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(time, CXMPPX_STANZA_NAME_TIME);
  xmpp_stanza_set_attribute(time, CXMPPX_STANZA_ATTRIBUTE_STAMP , buf);
  // rpad
  xmpp_stanza_t *rpad = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(rpad, CXMPPX_STANZA_NAME_RPAD);
  xmpp_stanza_t *rpad_text = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_text(rpad_text, rpad_data);
  // payload
  xmpp_stanza_t *payload = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(payload, CXMPPX_STANZA_NAME_PAYLOAD);
  // body
  xmpp_stanza_t *body = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(body, CXMPPX_STANZA_NAME_BODY);
  xmpp_stanza_set_ns(body, CXMPPX_NS_JABBER_CLIENT);
 // text
  xmpp_stanza_t *body_text = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_text(body_text, message);
  xmpp_stanza_add_child(signcrypt, s_to);
  xmpp_stanza_add_child(signcrypt, time);
  xmpp_stanza_add_child(signcrypt, rpad);
  xmpp_stanza_add_child(rpad, rpad_text);
  xmpp_stanza_add_child(signcrypt, payload);
  xmpp_stanza_add_child(payload, body);
  xmpp_stanza_add_child(body, body_text);

  return signcrypt;

}

