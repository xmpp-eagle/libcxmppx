/*!
 * @file openpgp.c
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "openpgp.h"
#include <locale.h>
#include <string.h>
#include "glib.h"

static char* _openpgp_signcrypt(gpgme_key_t sender, gpgme_key_t recipients[], char* message, char** errormsg);

gpgme_key_t openpgp_lookup_key(char* name) {
  gpgme_check_version (NULL);
  gpgme_set_locale (NULL, LC_CTYPE, setlocale (LC_CTYPE, NULL));
  gpgme_ctx_t ctx;
  gpgme_error_t error = gpgme_new (&ctx);
  gpgme_key_t key;

  GString *xmpp_uri = g_string_new("xmpp:");
  g_string_append(xmpp_uri, name);

  error = gpgme_op_keylist_start(ctx, NULL, 0);
  while (!error) {
    error = gpgme_op_keylist_next(ctx, &key);
    if (!error) {
      gpgme_user_id_t uids = key->uids;
      while (uids) {
        if (strcmp(uids->name, xmpp_uri->str) == 0) { 
          return key;
        }
        uids = uids->next;
      }
    } else {
      gpgme_key_release(key);
    }
  }
  return NULL;
}

char* openpgp_signcrypt(gpgme_key_t sender, gpgme_key_t recipients[], char* message, char** errormsg){ 
  return _openpgp_signcrypt(sender,recipients,message, errormsg);
}

static char* _openpgp_signcrypt(gpgme_key_t sender, gpgme_key_t recipients[], char* message, char** errormsg) {
  setlocale (LC_ALL, "");
  gpgme_check_version (NULL);
  gpgme_set_locale (NULL, LC_CTYPE, setlocale (LC_CTYPE, NULL));
  gpgme_ctx_t ctx;
  gpgme_error_t error = gpgme_new (&ctx);
  if(GPG_ERR_NO_ERROR != error ) {
    *errormsg = gpgme_strerror(error);
    return NULL;
  }
  error = gpgme_set_protocol(ctx, GPGME_PROTOCOL_OPENPGP);
  if(error != 0) {
    *errormsg = gpgme_strerror(error);
  }
  gpgme_set_armor(ctx,0);
  gpgme_set_textmode(ctx,0);
  gpgme_set_offline(ctx,1);
  gpgme_set_keylist_mode(ctx, GPGME_KEYLIST_MODE_LOCAL);

  if(error != 0) {
    *errormsg = gpgme_strerror(error);
    return NULL;
  }

  gpgme_key_t recp[3];
  recp[0] = sender,
  recp[1] = recipients[0];
  recp[2] = NULL;

  gpgme_signers_clear(ctx);
  error = gpgme_signers_add(ctx, recp[0]);
  if(error != 0) {
    *errormsg = gpgme_strerror(error);
    return NULL;
  }
  
  gpgme_encrypt_flags_t flags = 0;

  gpgme_data_t plain;
  gpgme_data_t cipher;
  
  error = gpgme_data_new(&plain);
  if(error != 0) {
    *errormsg = gpgme_strerror(error);
    return NULL;
  }
  
  error = gpgme_data_new_from_mem(&plain, message, strlen(message), 0);
  if(error != 0) {
    *errormsg = gpgme_strerror(error);
    return NULL;
  }
  error = gpgme_data_new(&cipher);
  if(error != 0) {
    *errormsg = gpgme_strerror(error);
    return NULL;
  }
  error = gpgme_op_encrypt_sign(ctx, recp, flags, plain, cipher);
  if(error != 0) {
    *errormsg = gpgme_strerror(error);
    return NULL;
  }
  size_t len;
  char *cipher_str = gpgme_data_release_and_get_mem(cipher, &len);
  char *result = g_base64_encode((unsigned char *)cipher_str, len);

}

