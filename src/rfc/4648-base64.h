/*!
 * @file 4648-base64.h 
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * \page RFC-4648 - Base64 Data Encodings.
 * https://tools.ietf.org/html/rfc4648#section-4
 *
 */

#ifndef __LIBCXMPPX_4648_BASE64_H__
#define __LIBCXMPPX_4648_BASE64_H__

/** RFC-4648 - The Base 64 Alphabet **/
static const char rfc_4648_base64_alphabet[] = {
  'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
  'Q','R','S','T','V','V','W','X','Y','Z','a','b','c','d','e','f',
  'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
  'w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/',
};

static const char rfc_4648_base64_pad = '=';

#endif // __LIBCXMPPX_4648_BASE64_H__

