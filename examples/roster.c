/*!
 * @file XEP-0030-disco.c
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// gcc -v ../../eagle/libcxmppx/libstrophe/.libs/libstrophe.so `gpgme-config --libs --cflags`  -I../ ../.libs/libcxmppx.so -o roster roster.c
// export LD_LIBRARY_PATH=../.libs/
// ./roster <jid> <pwd> 

#include <stdlib.h>
#include <stdio.h>
#include <cxmppx.h>

void display(xmpp_stanza_t *stanza, void *const obj, void *const userdata ) {
  cxmppx_stanza_t s;  
  s.stanza = stanza;
  cxmppx_roster_t* roster = cxmppx_build_roster(userdata, &s);
  cxmppx_xmpp_entity_t* entity = cxmppx_roster_items(roster);
  while ( entity ) {
  printf("Roster1: %s\n", cxmppx_xmpp_entity_get_xmppadr( entity ) );
    entity = cxmppx_xmpp_entity_next(entity);
  }
}

void conn_handler(xmpp_conn_t *const conn, const xmpp_conn_event_t status,
                  const int error, xmpp_stream_error_t *const stream_error,
                  void *const userdata) {
  switch (status) {
    case XMPP_CONN_CONNECT:
      printf("Connected!\n");
      // Create account
      cxmppx_account_t* account = cxmppx_create_account(conn);

      register_handler(conn, account);

      cxmppx_stanza_t presence;
      cxmppx_presence(&presence, conn);
      fire_and_handle(conn, &presence, NULL, NULL);

      cxmppx_stanza_t carbone;
      cxmppx_carbone(&carbone, conn);
      fire_and_handle(conn, &carbone, NULL, NULL);

      // query roster
      cxmppx_stanza_t cxmppx_stanza;
      cxmppx_roster_query_get(&cxmppx_stanza, conn);
      // fire query
      fire_and_handle(conn, &cxmppx_stanza, display, account);

    
      break;
    case XMPP_CONN_DISCONNECT:
    case XMPP_CONN_FAIL:
      xmpp_stop(xmpp_conn_get_context(conn));
      break;
  }

}

int main(int argc, char* argv[]) {
  xmpp_log_t *log = xmpp_get_default_logger(XMPP_LEVEL_DEBUG);
  xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, log);
  xmpp_conn_t *conn = xmpp_conn_new(ctx);
  xmpp_conn_set_jid(conn, argv[1]);
  xmpp_conn_set_pass(conn, argv[2]);

  int e =  xmpp_connect_client(conn, NULL, 0, conn_handler, argv[3]);
  if(XMPP_EOK != e ) {
    printf("xmpp_connect_client failed");
  }

  xmpp_run(ctx);
  // xmpp_disconnect(conn);
  xmpp_conn_release(conn);
  xmpp_ctx_free(ctx);
  xmpp_shutdown();

  return EXIT_SUCCESS;
}

