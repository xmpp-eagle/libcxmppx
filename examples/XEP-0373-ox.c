/*!
 * @file XEP-0373-ox.c
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @authors
 * Copyright (C) 2020 Stefan Kropp <stefan@debxwoody.de> 
 *
 * @copyright
 * This file is part of cxmppx.
 *
 * cxmppx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cxmppx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cxmppx.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// gcc -v `pkg-config --cflags --libs libstrophe` `gpgme-config --libs --cflags`  -I../ ../.libs/libcxmppx.so -o XEP-0373-ox  XEP-0373-ox.c
// export LD_LIBRARY_PATH=../.libs/
// ./XEP-0373-ox <jid> <pwd> <jid>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cxmppx.h>

static char* to_adr = NULL;

static xmpp_conn_t *conn = NULL;

void handle(xmpp_stanza_t *stanza, void *const obj, void *const userdata ) {
  printf("%s %s\n", userdata, obj);
  xmpp_disconnect(conn);

}

void conn_handler(xmpp_conn_t *const conn, const xmpp_conn_event_t status,
                  const int error, xmpp_stream_error_t *const stream_error,
                  void *const userdata) {
  switch (status) {
    case XMPP_CONN_CONNECT:
      printf("Connected!\n");

      cxmppx_stanza_t x;

      cxmppx_ox_discover_metadata(&x, conn, to_adr);
      fire_and_handle(conn, &x, handle, "Anfrage 1" );

      break;
    case XMPP_CONN_DISCONNECT:
    case XMPP_CONN_FAIL:
      xmpp_stop(xmpp_conn_get_context(conn));
      break;
  }

}

int main(int argc, char* argv[]) {
  xmpp_log_t *log = NULL;
  log = xmpp_get_default_logger(XMPP_LEVEL_DEBUG);
  xmpp_ctx_t *ctx = xmpp_ctx_new(NULL, log);
  conn = xmpp_conn_new(ctx);
  xmpp_conn_set_jid(conn, argv[1]);
  xmpp_conn_set_pass(conn, argv[2]);

  to_adr = strdup(argv[3]);

  int e =  xmpp_connect_client(conn, NULL, 0, conn_handler, NULL);
  if(XMPP_EOK != e ) {
    printf("xmpp_connect_client failed");
  }

  xmpp_run(ctx);
  xmpp_conn_release(conn);
  xmpp_ctx_free(ctx);
  xmpp_shutdown();

  return EXIT_SUCCESS;
}

