# libcxmppx - A XMPP XEP C lib

Make XMPP XEP development easy.

This C library will provide the implementation of XMPP-XEPs on top of
[libstrophe](https://strophe.im/libstrophe/).

* see: cxmppx.h
* examples/

## XEPs

* [XEP-0030 - disco](https://xmpp.org/extensions/xep-0030.html)
* [XEP-0060 - pubsub](https://xmpp.org/extensions/xep-0060.html)
* [XEP-0082 - time](https://xmpp.org/extensions/xep-0082.html)
* [XEP-0373 - ox](https://xmpp.org/extensions/xep-0373.html)
* [XEP-0420 - sce](https://xmpp.org/extensions/xep-0420.html)

